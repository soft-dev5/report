/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.albumproject.service;

import com.nakamol.albumproject.dao.ReportSaleDao;
import com.nakamol.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author OS
 */
public class ReportSaleService {

    public List<ReportSale> getReportSaleByDay() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport(year);
    }
}
